using WebApplicationSecurityProject22.Domain.identity;

namespace WebApplicationSecurityProject22.Domain
{
    public class DiffieHelman
    {
        public int Id { get; set; }
        // base number
        public long PublicKeyG { get; set; }
        //prime(mod)
        public long PublicKeyP { get; set; }
        
        public long PrivateKeyA { get; set; }
        public long PrivateKeyB { get; set;  }
        
        
        public long SharedSecret { get; set;  }
        public string PlainText { get; set; } = default!;
        public byte[] CypherText { get; set; }
        
        public string AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
    }
}