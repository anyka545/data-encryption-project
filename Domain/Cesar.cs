
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using WebApplicationSecurityProject22.Domain.identity;

namespace WebApplicationSecurityProject22.Domain
{

// generate crud pages
// dotnet aspnet-codegenerator controller -name RSAController -actions -m RSA -dc ApplicationDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f

    public class Cesar
    {
        // Auto-detected primary key
        // dotnet aspnet-codegenerator controller -name OwnersController -actions -m Owner -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f

        public int Id { get; set; }

        public int ShiftAmount { get; set; }

        public string PlainText { get; set; } = default!;
        public string CipherText { get; set;  }
        
        public string AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
        
    }
}