
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace WebApplicationSecurityProject22.Domain.identity
{
    public class AppUser : IdentityUser
    {
        public ICollection<Cesar>? Cesar { get; set; }
    }
}