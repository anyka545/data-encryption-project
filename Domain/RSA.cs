using WebApplicationSecurityProject22.Domain.identity;

namespace WebApplicationSecurityProject22.Domain
{
    public class RSA
    {
        public int Id { get; set; }
        // base number(prime)
        public long primeP { get; set; }
        //prime(mod)
        public long primeQ { get; set; }
        
        public long PublicKeyN { get; set; }
        public long PublicKeyE { get; set;  }
        
        public string PlainText { get; set; } = default!;
        public byte[] CypherText { get; set; }
        
        public string AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
    }
}