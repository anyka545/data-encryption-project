﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplicationSecurityProject22.Domain;
using WebApplicationSecurityProject22.Domain.identity;

namespace WebApplicationSecurityProject22.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Cesar> Cesars { get; set; } = default!;
        public DbSet<DiffieHelman> DiffieHelman { get; set; } = default!;
        public DbSet<RSA> RSA { get; set; } = default!;
    }
}