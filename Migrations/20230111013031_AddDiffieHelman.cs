﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplicationSecurityProject22.Migrations
{
    public partial class AddDiffieHelman : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiffieHelman",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PublicKeyG = table.Column<int>(type: "INTEGER", nullable: false),
                    PublicKeyP = table.Column<int>(type: "INTEGER", nullable: false),
                    PrivateKeyA = table.Column<int>(type: "INTEGER", nullable: false),
                    PrivateKeyB = table.Column<int>(type: "INTEGER", nullable: false),
                    SharedSecret = table.Column<int>(type: "INTEGER", nullable: false),
                    PlainText = table.Column<string>(type: "TEXT", nullable: true),
                    CypherText = table.Column<string>(type: "TEXT", nullable: true),
                    AppUserId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiffieHelman", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiffieHelman_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RSA",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    primeP = table.Column<int>(type: "INTEGER", nullable: false),
                    primeQ = table.Column<int>(type: "INTEGER", nullable: false),
                    PublicKeyN = table.Column<int>(type: "INTEGER", nullable: false),
                    PublicKeyE = table.Column<int>(type: "INTEGER", nullable: false),
                    PlainText = table.Column<string>(type: "TEXT", nullable: true),
                    CypherText = table.Column<string>(type: "TEXT", nullable: true),
                    AppUserId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RSA", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RSA_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiffieHelman_AppUserId",
                table: "DiffieHelman",
                column: "AppUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RSA_AppUserId",
                table: "RSA",
                column: "AppUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiffieHelman");

            migrationBuilder.DropTable(
                name: "RSA");
        }
    }
}
