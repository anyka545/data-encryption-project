using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplicationSecurityProject22.Data;
using WebApplicationSecurityProject22.Domain;

namespace WebApplicationSecurityProject22.Controllers
{
    [Authorize]
    public class RSAController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RSAController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RSA
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.RSA
                    .Where(c => c.AppUserId == GetUserLoginId())
                .Include(r => r.AppUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: RSA/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }

            var rSA = await _context.RSA
                .Include(r => r.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rSA == null)
            {
                return NotFound();
            }

            return View(rSA);
        }

        // GET: RSA/Create
        public IActionResult Create()
        {
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }
        public string GetUserLoginId()
        {
            return User.Claims.First(cm => cm.Type == ClaimTypes.NameIdentifier).Value;
        }
        static long GCD(long a, long b)
        {
            if (a == 0) return b;
            return GCD(b % a, a);
        }


        // POST: RSA/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( RSA rSA)
        {
            bool boolean = ValidateIsPrime(rSA.primeP);
            bool boolean1 = ValidateIsPrime(rSA.primeQ);
            if (boolean == false)
            {
                rSA.primeP = FindTheBiggestPrimeUnderSpecifiedValue(rSA.primeP);

            }
            if (boolean1 == false)
            {
                rSA.primeQ = FindTheBiggestPrimeUnderSpecifiedValue(rSA.primeQ);

            }
            rSA.PublicKeyN = rSA.primeP * rSA.primeQ;
            var m = (rSA.primeP-1) * (rSA.primeQ-1);
            rSA.PublicKeyE = 1;
            long gcd = 0;
            do
            {
                rSA.PublicKeyE++;
                gcd = GCD(m, rSA.PublicKeyE);
            } while (gcd != 1 && rSA.PublicKeyE < m);

            if (rSA.PublicKeyE > m)
            {
                throw new ApplicationException("e > m");
            }
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rSA.PlainText!);
            long[] bytesAsInts = plainTextBytes.Select(x => (long)x).ToArray();
            byte[] intBytes = new Byte[64];
            for (var i = 0; i < plainTextBytes.Length-1; i++)
                
            {
                bytesAsInts[i] = FastModularExponentiation(bytesAsInts[i], rSA.PublicKeyE, rSA.PublicKeyN);
                intBytes = BitConverter.GetBytes(bytesAsInts[i]);
            }
            var b64Text =Convert.ToBase64String(intBytes);

            rSA.CypherText = System.Convert.FromBase64String(b64Text);


            
            rSA.AppUserId = GetUserLoginId();
            if (ModelState.IsValid)
            {
                _context.Add(rSA);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", rSA.AppUserId);
            return View(rSA);
        }

        // GET: RSA/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rSA = await _context.RSA.FindAsync(id);
            if (rSA == null)
            {
                return NotFound();
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", rSA.AppUserId);
            return View(rSA);
        }
      
        public static bool ValidateIsPrime(long prime)
        {
            int i = 2;
            int k = (int) Math.Ceiling(Math.Sqrt(prime));
            while (i <= k)
            {
                if (prime % i == 0)
                    return false;
                i += 1;
            }

            return true;
        }

        public static long FindTheBiggestPrimeUnderSpecifiedValue(long prime)
        {
            bool num = false;
            while (num == false)
            {
                prime = prime - 1;
                num = ValidateIsPrime(prime);
            }


            return prime;
        }
        public static long FastModularExponentiation(long baseNum, long power, long mod)
        {
            if (power == 0)
            {
                return 1;
            }

            if (power % 2 == 0)
            {
                var p = FastModularExponentiation(baseNum, power / 2, mod);
                return p * p % mod;
            }
            else
            {
                return baseNum * FastModularExponentiation(baseNum, power - 1, mod) % mod;
            }
        }
        // POST: RSA/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,primeP,primeQ,PublicKeyN,PublicKeyE,PlainText,CypherText,AppUserId")] RSA rSA)
        {
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            bool boolean = ValidateIsPrime(rSA.primeP);
            bool boolean1 = ValidateIsPrime(rSA.primeQ);
            if (boolean == false)
            {
                rSA.primeP = FindTheBiggestPrimeUnderSpecifiedValue(rSA.primeP);

            }
            if (boolean1 == false)
            {
                rSA.primeQ = FindTheBiggestPrimeUnderSpecifiedValue(rSA.primeQ);

            }
            rSA.PublicKeyN = rSA.primeP * rSA.primeQ;
            var m = (rSA.primeP-1) * (rSA.primeQ-1);
            rSA.PublicKeyE = 1;
            long gcd = 0;
            do
            {
                rSA.PublicKeyE++;
                gcd = GCD(m, rSA.PublicKeyE);
            } while (gcd != 1 && rSA.PublicKeyE < m);

            if (rSA.PublicKeyE > m)
            {
                throw new ApplicationException("e > m");
            }
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rSA.PlainText!);
            long[] bytesAsInts = plainTextBytes.Select(x => (long)x).ToArray();
            byte[] intBytes = new Byte[64];
            for (var i = 0; i < plainTextBytes.Length-1; i++)
                
            {
                bytesAsInts[i] = FastModularExponentiation(bytesAsInts[i], rSA.PublicKeyE, rSA.PublicKeyN);
                intBytes = BitConverter.GetBytes(bytesAsInts[i]);
            }
            var b64Text =Convert.ToBase64String(intBytes);

            rSA.CypherText = System.Convert.FromBase64String(b64Text);
            rSA.AppUserId = GetUserLoginId();


            
            if (id != rSA.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rSA);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RSAExists(rSA.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", rSA.AppUserId);
            return View(rSA);
        }

        // GET: RSA/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rSA = await _context.RSA
                .Include(r => r.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rSA == null)
            {
                return NotFound();
            }

            return View(rSA);
        }

        // POST: RSA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            var rSA = await _context.RSA.FindAsync(id);
            _context.RSA.Remove(rSA);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RSAExists(int id)
        {
            return _context.RSA.Any(e => e.Id == id);
        }
    }
}
