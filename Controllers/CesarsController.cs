using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplicationSecurityProject22.Data;
using WebApplicationSecurityProject22.Domain;

namespace WebApplicationSecurityProject22.Controllers
{
    
    [Authorize]
    public class CesarsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CesarsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Cesars
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context
                .Cesars
                .Where(c => c.AppUserId == GetUserLoginId())
                .Include(c => c.AppUser);
            return View(await applicationDbContext.ToListAsync());
        }

        public string GetUserLoginId()
        {
            return User.Claims.First(cm => cm.Type == ClaimTypes.NameIdentifier).Value;
        }

        // GET: Cesars/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }

            var cesar = await _context.Cesars
                .Include(c => c.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cesar == null)
            {
                return NotFound();
            }

            return View(cesar);
        }

        // GET: Cesars/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cesars/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Cesar cesar)
        {
            cesar.CipherText = GetCipherText(cesar.PlainText, cesar.ShiftAmount);
            cesar.AppUserId= GetUserLoginId(); 
            if (ModelState.IsValid)
            {
                _context.Add(cesar);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", cesar.AppUserId);
            return View(cesar);
        }

        public string GetCipherText(string plainText, int shiftamount)
        {
            var defaultAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?.,";
            var plainTextFixed = "";
            if (plainText != null)
            {
                foreach (var chr in plainText)
                {
                    if (defaultAlphabet.Contains(chr))
                    {
                        plainTextFixed += chr;
                    }
                }

                var encryptedText = "";
                foreach (var chr in plainTextFixed)
                {
                    var pos =
                        (defaultAlphabet.IndexOf(chr) + shiftamount) % defaultAlphabet.Length;
                    pos = pos < 0 ? pos + defaultAlphabet.Length : pos;

                    encryptedText = encryptedText + defaultAlphabet[pos];

                }

                return encryptedText;
            }
            return plainTextFixed;
        }

        // GET: Cesars/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Cesars == null)
            {
                return NotFound();
            }
            

            var cesar = await _context.Cesars.
                SingleOrDefaultAsync(m => m.AppUserId == GetUserLoginId() && m.Id == id);
            if (cesar == null)
            {
                return NotFound();
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", cesar.AppUserId);
            return View(cesar);
        }

        // POST: Cesars/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Cesar cesar)
        {
            if (id != cesar.Id)
            {
                return NotFound();
            }
            
            cesar.CipherText = GetCipherText(cesar.PlainText, cesar.ShiftAmount);

            cesar.AppUserId = GetUserLoginId();
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cesar);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CesarExists(cesar.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", cesar.AppUserId);
            return View(cesar);
        }

        // GET: Cesars/Delete/5
        public async Task<IActionResult> Delete(int? id, Cesar ces)
        {
            if (id != ces.Id)
            {
                return NotFound();
            }
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }

            var cesar = await _context.Cesars
                .Include(c => c.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cesar == null)
            {
                return NotFound();
            }

            return View(cesar);
        }
        

        // POST: Cesars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            var cesar = await _context.Cesars.FindAsync(id);
            _context.Cesars.Remove(cesar);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CesarExists(int id)
        {
            return _context.Cesars.Any(e => e.Id == id);
        }
    }
}
