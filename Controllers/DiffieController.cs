using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplicationSecurityProject22.Data;
using WebApplicationSecurityProject22.Domain;

namespace WebApplicationSecurityProject22.Controllers
{
    [Authorize]
    public class DiffieController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DiffieController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Diffie
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DiffieHelman
                .Where(c => c.AppUserId == GetUserLoginId())
                .Include(d => d.AppUser);
            return View(await applicationDbContext.ToListAsync());
        }
        public string GetUserLoginId()
        {
            return User.Claims.First(cm => cm.Type == ClaimTypes.NameIdentifier).Value;
        }
        // GET: Diffie/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }

            var diffieHelman = await _context.DiffieHelman
                .Include(d => d.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diffieHelman == null)
            {
                return NotFound();
            }

            return View(diffieHelman);
        }

        // GET: Diffie/Create
        public IActionResult Create()
        {
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: Diffie/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        public static bool ValidateIsPrime(long prime)
        {
            int i = 2;
            int k = (int) Math.Ceiling(Math.Sqrt(prime));
            while (i <= k)
            {
                if (prime % i == 0)
                    return false;
                i += 1;
            }

            return true;
        }

        public static long FindTheBiggestPrimeUnderSpecifiedValue(long prime)
        {
            bool num = false;
            while (num == false)
            {
                prime = prime - 1;
                num = ValidateIsPrime(prime);
            }


            return prime;
        }
        public static long FastModularExponentiation(long baseNum, long power, long mod)
        {
            if (power == 0)
            {
                return 1;
            }

            if (power % 2 == 0)
            {
                var p = FastModularExponentiation(baseNum, power / 2, mod);
                return p * p % mod;
            }
            else
            {
                return baseNum * FastModularExponentiation(baseNum, power - 1, mod) % mod;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DiffieHelman diffieHelman)
        {
            bool boolean = ValidateIsPrime(diffieHelman.PublicKeyP);
            if (boolean == false)
            {
                diffieHelman.PublicKeyP = FindTheBiggestPrimeUnderSpecifiedValue(diffieHelman.PublicKeyP);

            }

            if (diffieHelman.PrivateKeyA >= diffieHelman.PublicKeyP)
            {
                diffieHelman.PrivateKeyA = diffieHelman.PrivateKeyA % diffieHelman.PublicKeyP;
            }
            if (diffieHelman.PrivateKeyB >= diffieHelman.PublicKeyP)
            {
                diffieHelman.PrivateKeyB = diffieHelman.PrivateKeyB % diffieHelman.PublicKeyP;
            }
            var computeA = FastModularExponentiation(diffieHelman.PublicKeyG, diffieHelman.PrivateKeyA, diffieHelman.PublicKeyP);
            //var computeB = FastModularExponentiation(diffieHelman.PublicKeyG, diffieHelman.PrivateKeyB, diffieHelman.PublicKeyP);
            diffieHelman.SharedSecret = FastModularExponentiation(computeA, diffieHelman.PrivateKeyB, diffieHelman.PublicKeyP);
            //plain text ecryption:
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(diffieHelman.PlainText!);
            long[] bytesAsInts = plainTextBytes.Select(x => (long)x).ToArray();
            byte[] intBytes = new Byte[64];
            for (var i = 0; i < plainTextBytes.Length-1; i++)
                
            {
                bytesAsInts[i] = FastModularExponentiation(bytesAsInts[i], diffieHelman.SharedSecret, diffieHelman.PublicKeyP);
                intBytes = BitConverter.GetBytes(bytesAsInts[i]);
            }
            var b64Text =Convert.ToBase64String(intBytes);

            diffieHelman.CypherText = System.Convert.FromBase64String(b64Text);

            diffieHelman.AppUserId= GetUserLoginId(); 
            if (ModelState.IsValid)
            {
                _context.Add(diffieHelman);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", diffieHelman.AppUserId);
            return View(diffieHelman);
        }

        // GET: Diffie/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diffieHelman = await _context.DiffieHelman.FindAsync(id);
            if (diffieHelman == null)
            {
                return NotFound();
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", diffieHelman.AppUserId);
            return View(diffieHelman);
        }

        // POST: Diffie/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, DiffieHelman diffieHelman)
        {
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            bool boolean = ValidateIsPrime(diffieHelman.PublicKeyP);
            if (boolean == false)
            {
                diffieHelman.PublicKeyP = FindTheBiggestPrimeUnderSpecifiedValue(diffieHelman.PublicKeyP);

            }

            if (diffieHelman.PrivateKeyA >= diffieHelman.PublicKeyP)
            {
                diffieHelman.PrivateKeyA = diffieHelman.PrivateKeyA % diffieHelman.PublicKeyP;
            }
            if (diffieHelman.PrivateKeyB >= diffieHelman.PublicKeyP)
            {
                diffieHelman.PrivateKeyB = diffieHelman.PrivateKeyB % diffieHelman.PublicKeyP;
            }
            var computeA = FastModularExponentiation(diffieHelman.PublicKeyG, diffieHelman.PrivateKeyA, diffieHelman.PublicKeyP);
            //var computeB = FastModularExponentiation(diffieHelman.PublicKeyG, diffieHelman.PrivateKeyB, diffieHelman.PublicKeyP);
            diffieHelman.SharedSecret = FastModularExponentiation(computeA, diffieHelman.PrivateKeyB, diffieHelman.PublicKeyP);
            //plain text ecryption:
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(diffieHelman.PlainText!);
            long[] bytesAsInts = plainTextBytes.Select(x => (long)x).ToArray();
            byte[] intBytes = new Byte[64];
            for (var i = 0; i < plainTextBytes.Length-1; i++)
                
            {
                bytesAsInts[i] = FastModularExponentiation(bytesAsInts[i], diffieHelman.SharedSecret, diffieHelman.PublicKeyP);
                intBytes = BitConverter.GetBytes(bytesAsInts[i]);
            }
            var b64Text =Convert.ToBase64String(intBytes);

            diffieHelman.CypherText = System.Convert.FromBase64String(b64Text);
            diffieHelman.AppUserId = GetUserLoginId();
            if (id != diffieHelman.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diffieHelman);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiffieHelmanExists(diffieHelman.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "Id", diffieHelman.AppUserId);
            return View(diffieHelman);
        }

        // GET: Diffie/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diffieHelman = await _context.DiffieHelman
                .Include(d => d.AppUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diffieHelman == null)
            {
                return NotFound();
            }

            return View(diffieHelman);
        }

        // POST: Diffie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var isOwned = await _context.Cesars.AnyAsync(c => c.Id == id && c.AppUserId == GetUserLoginId());
            if (!isOwned)
            {
                return NotFound();
            }
            var diffieHelman = await _context.DiffieHelman.FindAsync(id);
            _context.DiffieHelman.Remove(diffieHelman);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DiffieHelmanExists(int id)
        {
            return _context.DiffieHelman.Any(e => e.Id == id);
        }
    }
}
